package com.darmawan.simplegame;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class MainActivity extends AppCompatActivity {
    GameView gameView;

    DisplayMetrics displayMetrics = new DisplayMetrics();
    int widthScreen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

        gameView = new GameView(this);
        setContentView(gameView);
    }

    class GameView extends SurfaceView implements Runnable {
        Thread gameThread = null;
        SurfaceHolder ourHolder;
        volatile boolean playing;
        Canvas canvas;
        Paint paint;
        long fps;
        private long timeThisFrame;
        Bitmap bitmapBob;
        boolean isMoving = false;
        float walkSpeedPerSecond = 150;
        float bobXPosition = 10;
        boolean flip = true;

        public GameView(Context context) {
            super(context);
            ourHolder = getHolder();
            paint = new Paint();
            bitmapBob = BitmapFactory.decodeResource(this.getResources(), R.drawable.character);
            playing = true;
            widthScreen = displayMetrics.widthPixels - bitmapBob.getWidth();
        }

        @Override
        public void run() {
            while (playing) {
                long startFrameTime = System.currentTimeMillis();
                update();
                draw();
                timeThisFrame = System.currentTimeMillis() - startFrameTime;
                if (timeThisFrame > 0) {
                    fps = 1000 / timeThisFrame;
                }
            }
        }

        public void update() {
            if (isMoving) {
                if(bobXPosition < 0f || bobXPosition > widthScreen) {
                    flip = bobXPosition < 0f ? true : false;
                    walkSpeedPerSecond = -walkSpeedPerSecond;
                }

                bobXPosition = bobXPosition + (walkSpeedPerSecond / fps);
            }
        }

        public void draw() {
            if (ourHolder.getSurface().isValid()) {
                canvas = ourHolder.lockCanvas();

                canvas.drawColor(Color.argb(255, 255, 255, 255));
                paint.setColor(Color.argb(255, 249, 129, 0));
                paint.setTextSize(45);
                canvas.drawText("FPS:" + fps, 20, 55, paint);
                canvas.drawBitmap(flip ? FlipHorizontally(bitmapBob) : bitmapBob, bobXPosition, 300, paint);
                ourHolder.unlockCanvasAndPost(canvas);
            }
        }

        public Bitmap FlipHorizontally(Bitmap originalImage) {
            int width = originalImage.getWidth();
            int height = originalImage.getHeight();

            Matrix matrix = new Matrix();
            matrix.preScale(-1, 1);

            Bitmap flipImage = Bitmap.createBitmap(originalImage, 0,0 , width, height, matrix, true);

            return flipImage;
        }

        public void pause() {
            playing = false;
            try {
                gameThread.join();
            } catch (InterruptedException e) {
                Log.e("Error:", "joining thread");
            }
        }

        public void resume() {
            playing = true;
            gameThread = new Thread(this);
            gameThread.start();
        }

        @Override
        public boolean onTouchEvent(MotionEvent motionEvent) {
            switch (motionEvent.getAction() & MotionEvent.ACTION_MASK) {
                case MotionEvent.ACTION_DOWN:
                    isMoving = true;
                    break;
                case MotionEvent.ACTION_UP:
                    isMoving = false;
                    break;
            }
            return true;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        gameView.resume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        gameView.pause();
    }
}
